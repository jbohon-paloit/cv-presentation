require('./config/config');

const fs = require('fs');
const {google} = require('googleapis');
const async = require('async');
const axios = require('axios');
const express = require('express');
const Git = require('nodegit');

const key = require('./auth.json');

const app = express();
// const port = process.env.PORT || 3000;
const port = 8080;
const HOST = 'localhost';


const permissionEmail = process.env.PERMISSION_ADDRESS || '';

const SCOPES = ['https://www.googleapis.com/auth/drive'];
const jwt = new google.auth.JWT(key.client_email, null, key.private_key, SCOPES);

const permissionsToAdd = [{
  'type': 'user',
  'role': 'writer',
  'emailAddress': permissionEmail
}];

// Auth with async function
// const auth = async (jwt) => {
//   const drive = await jwt.authorize((err, response) => {
//     console.log('auth');
//     return google.drive({version: 'v3', auth: jwt});
//   });
//   return drive;
// };

const path = require('path');
const url = "https://github.com/massardc/cv-fact.git",
    local = "./resumes";

Git.Clone(url, local).then((repo) => {
  axios.get('/files', { proxy: { host: '127.0.0.1', port: 8081 }})
    .then(response => {
      console.log('RRR', response.data.url);
      console.log('RRR', response.data);
    }).catch(error => {
      console.log(error);
    });
  console.log("Cloned " + path.basename(url) + " to " + repo.workdir());
  return repo;
}).then((repo) => {
  console.log('repo => ', repo);
  fs.readdir(repo.workdir(), (err, files) => {
    if (err) {
      console.log('Err =>', err);
    }
    files.forEach(file => {
      console.log(file);
      if (path.extname(file) === '.md') {
        console.log('MD File');
        axios.post(`/files/${file}`, { proxy: { host: '127.0.0.1', port: 8081 }})
          .then(response => {
            console.log('Posted', response.data);
          }).catch(error => {
            console.log('E', error);
          });
      }
    });
  })
}).catch(function (err) {
  console.log(err);
});

// POST /files -- File push to Google Drive
app.post('/files/:fileName', (req, res) => {
  const fileName = req.params.fileName;
  if (!fileName) {
    return res.status(400).send({
      error: 'File name is incorrect.'
    });
  }
  jwt.authorize(async (err) => {
    const drive = google.drive({version: 'v3', auth: jwt});
    const response = await postFiles(drive, fileName);

    if (response.status === 400) {
      return res.status(400).send(response.error.errors);
    }
    res.send(response.data);
  });
});

const postFiles = async (drive, fileName) => {
  const fileMetadata = {
    'name': fileName
  };

  try {
    const media = {
      // TODO: Useful to get mime type or always docx?
      // mimeType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
      mimeType: 'text/markdown',
      body: fs.createReadStream(`/cvfactory/resumes/${fileName}`)
    };

    const driveResponse = await drive.files.create({
      resource: fileMetadata,
      media: media,
      fields: 'id',
      writersCanShare: true
    });

    // Updating permission(s) right after file push to Drive throws an error (500)
    // Waiting 5 secs allows us to bypass that
    const perm = await setTimeout(() => {
      return updatePermission(drive, driveResponse.data.id);
    }, 5000); 

    if (perm.status === 400) {
      return {
        status: 400,
        error: perm.error
      };
    }
    return {
      status: 200,
      data: driveResponse.data.id
    };
  } catch (e) {
    return {
      status: 400,
      error: e
    };
  }
};

// GET /files -- Get files stored in Google Drive 
app.get('/files', (req, res) => {
  jwt.authorize(async (err, _) => {
    const drive = google.drive({version: 'v3', auth: jwt});
    const response = await getFiles(drive);

    if (response.status === 400) {
      return res.status(400).send(response.error.errors);
    }
    res.send(response.data);
  });
}); 

const getFiles = async (drive) => {
  try {
    const driveResponse = await drive.files.list({
      pageSize: 20,
      fields: 'nextPageToken, files(id, name, modifiedTime, permissions)',
    });
    return {
      status: 200,
      data: driveResponse.data.files
    };
  } catch (e) {
    return {
      status: 400,
      error: e
    };
  }
};

// Using the NPM module 'async'.
// Add permission to email associated in config/config.json
// So account has access to file.
const updatePermission = (drive, fileId) => {
  async.eachSeries(permissionsToAdd, function (permission, permissionCallback) {
    drive.permissions.create({
      resource: permission,
      fileId: fileId,
      fields: 'id',
    }, function (err, res) {
      if (err) {
        console.error(err);
        permissionCallback(err);
      } else {
        console.log('Permission ID: ', res.data.id)
        permissionCallback();
      }
    });
  }, function (err) {
    if (err) {
      return {
        status: 400,
        error: err
      };
    } else {
      return {
        status: 200
      };
    }
  });
};

app.get('/', (req, res) => {
  res.send('Hello world\n');
});


// app.listen(port, () => {
//   console.log(`Started up on port ${port}.`);
// });
app.listen(8081, function () {
  console.log('app listening on port 8081!')
})


// For testing purpose only, method not used
const deleteFile = (drive, fileId) => {
  drive.files.delete({
    'fileId': fileId
  }, function (err, file) {
    if (err) {
      // Handle error
      console.error(err);
    } else {
      console.log('File deleted ');
    }
  });
};