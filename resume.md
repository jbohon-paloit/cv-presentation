![](logo_website.png "Palo IT Logo"){ width=150px }

#### Paul Dubois

##### Développeur Java 

***

### PROFIL

Ceci est la description de Paul DUBOIS.

Paul est un excellent développeur. 

***

### COMPETENCES

* **Langages / Web** : Java, Angular 4, Spring Boot (Formation), HTML, Ajax, Bootstrap, Spring, Hibernate

* **OS** : Mac, Linux, Windows

***

### EXPERIENCES 

> **Octobre 2016 - Juin 2018 - Smile Open Source Solutions - Projet Vitam**

**Rôle**

**Java/Front End Développeur**

**Fonctions**

* Interventions sur la migration de framework front-end (AngularJS to Angular 4)

* Interfaces utilisateur, ajout de fonctionnalités et de tests

* Réalisation de tests unitaires et d'intégration pour les services back-end

**Environnement**

Java 8, Maven 3.3, Git 2.7, Docker 1.12, Angular 4, REST, Gulp, Mockito, Agile

**Jui 2016 à Septembre 2016 - Smile Open Source Solutions - Projet AMF**

**Rôle**

**Java/J2EE Développeur**

**Fonctions**

Création de nouveaux composants dans le CMS Magnolia permettant aux utilisateurs d'uploader du contenu média plus facilement

**Environnement**

Java 8, Maven 3.2, Tomcat 8, Magnolia CMS, PostgreSQL 9, JackRabbit 2.8, Freemarker

**Mars 2016 à Mai 2016 - Smile Open Source Solutions - Projet Cerba HealthCare**

***

### EDUCATION

Octobre 2015 :    **Master Science - Ingénieur logiciels et Gestion de projet**

*Université des Sciences et Technologies - Lille, France*

Cours suivis :

* Programmation Orientée Objet	

* Programmation Java

* Administration + optimisation de bases de données

* Data Mining/Warehousing

***

### LANGUES

* **Français** : Natif

* **Anglais** : Courant (C2, TOEIC 980) *- 6 mois aux US (2012)*

***

### ACTIVITES ET CENTRES D'INTERETS

**Technologies** :

* iOS développement

**Voyages**: 

* Corée, Japon, Chine, Hong Kong et États-Unis

**Sports** : 

* Sports d'équipe, handball (13 ans)

* Running
